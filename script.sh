#!/bin/bash
# Starts node script & handles restart in case of code 13 failure

echo >> myout
echo date >> myout

COUNT=1

node index.js > "$(date)-$COUNT.txt"
NODE_EXITED_WITH=$?

while [ $NODE_EXITED_WITH -eq 13 ] && [ $COUNT -lt 5 ]
do
  node index.js > "$(date)-$COUNT.txt"
  NODE_EXITED_WITH=$?
  (( COUNT++ ))
done

if [ $NODE_EXITED_WITH -eq 0 ]
then
  echo Recordings transcribed sucessfully. The script got executed $COUNT times >> myout
else
  echo Something went wrong. This script got executed $COUNT times >> myout
fi

echo --------------------------------------------- >> myout
