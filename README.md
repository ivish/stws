### Running Instructions for Mac/Linux based systems

If node.js isn't already installed on your system. Go ahead and [download][def1] the LTS version for your system.

Now clone this project to your Desktop or wherever you like. To do so, open your terminal and use these commands.

```sh
cd ~/Desktop
git clone https://bitbucket.org/ivish/stws.git
```

**Note**: If you get something like this while running second command *'git: command not found'*. [Download] the zip of project from github & extract to your Desktop.

Once cloned or downloaded, change your directory to the project's root directory using this command in your terminal

```sh
cd ~/Desktop/stws/
```

Once you are inside the project's root directory. Run this command in your terminal & you are good to go

```sh
npm start
```

The scripts will read the files from `/usr/local/freeswitch/recordings/<ip>` directory and put the transcripts in `dirtyTranscript` & `cleanTranscript` directory. After that script will go ahead and insert those transcripts in DB. Once the insertion is done, script deletes the transcription from `dirtyTranscript` & `cleanTranscript`. Time for this process to complete will depend on  the total number of minutes to be transcribed. You will be getting feedback about what's going on, inside your terminal during the whole lifecyle of the process.




   [def1]: <https://nodejs.org/en/>
   [Download]: <https://bitbucket.org/ivish/stws>
