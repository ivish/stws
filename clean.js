'use strict';

var getListFromDir = require('./dirFilesList').getListFromDir,
	path = require('path'),
	fs = require('fs'),
	iDD = require('./insertDataInDB');

exports.startCleaningTranscript = startCleaningTranscript;

var cleanDir;

function startCleaningTranscript(dirtyDir, cleanD) {
	cleanDir = cleanD;
	getListFromDir(dirtyDir)
	.then(function (filesList){
		var transcriptList = filesList.filter(function(elem, index, arr){
							if(elem.indexOf('.json') !== -1){
								return true;
							}
							else{
								return false;
							}
						});
		
		transcriptList.forEach(function(currentVal, index, arr) {
			console.log('Cleaning file ' + path.join(dirtyDir, currentVal));
			cleanFile(path.join(dirtyDir, currentVal));
		});
		console.log('')
		iDD.init(cleanDir);

	}, function(err){
		console.log(err);
	})
}

function cleanFile(filePath){
	var dirtyTranscription = require(filePath).results;
	if(dirtyTranscription) {
		var cleanTranscript = [];
		dirtyTranscription.forEach(function(currentVal, index, arr){
			if(currentVal && currentVal.alternatives && currentVal.alternatives[0]){
				cleanTranscript.push(currentVal.alternatives[0].transcript);
			} else {
				cleanTranscript.push(["transcribe failed"]);
			}
		});
		fs.writeFileSync(path.join(cleanDir, filePath.split('/').slice(-1).pop()), JSON.stringify({'result': cleanTranscript}));
		console.log(filePath + ' Cleaned');
	}
}
