'use strict';

var Websocket = require('ws'),
	getListFromDir = require('./dirFilesList').getListFromDir,
	path = require('path'),
	fs = require('fs'),
	cleaner = require('./clean'),
	callRecordingRegEx = /rec\d\_[\w\d\-]+\_rec\.wav/,
	tid;




exports.initSocket = initSocket;



function initSocket(options) {

	//#Narrowband
	var model = options.model ||
					'en-US_NarrowbandModel',
		token = options.token,
		wsURI = options.wsURI ||
					'wss://stream.watsonplatform.net/speech-to-text/api/v1/recognize?watson-token=' + token + '&model=' + model,
		firstMessage = options.firstMessage ||
		{
			'action': 'start',
			'content-type': 'audio/wav',
			'continuous': true,
			'inactivity_timeout': -1
		},
		jIndex = 0,
		ws,
		audioFilesList,
		isTranscriptionRecieved = true;


	firstMessage = JSON.stringify(firstMessage);


	getListFromDir(options.srcDirPath)
	.then(function(filesList) {
		audioFilesList = filesList.filter(function(elem, index, arr){
							if(callRecordingRegEx.test(elem)){
								return true;
							}
							else{
								return false;
							}
						});

		console.log('Audio files list to be transcribed ' + audioFilesList);
		ws = new Websocket(wsURI);
		ws.on('open', onOpen);

		ws.on('message', onMessage);

		ws.on('error', onError);

		ws.on('close', onClose);
	}, function(err) {
		console.log(err);
		process.exit(-1);
	});



	function onOpen() {
		ws.send(firstMessage);
		var noOpMessage = JSON.stringify({'action': 'no-op'});
		tid = setInterval(function() {
				ws.send(noOpMessage)
		}, 20000);
	}


	function onMessage(message) {
		if(JSON.parse(message).state === 'listening'){
			if(jIndex < audioFilesList.length){
				readAndSendFile(path.join(options.srcDirPath, audioFilesList[jIndex]), ws);
			}
			else{
				clearInterval(tid);
				ws.close();
			}
		}
		else{
			fs.writeFileSync(path.join(options.dirtyDirPath, audioFilesList[jIndex]).replace('.wav', '.json'), message);
			console.log(path.join(options.srcDirPath, audioFilesList[jIndex]) + ' file transcribed. ' + 'Transcription was saved in ' + path.join(options.dirtyDirPath, audioFilesList[jIndex]).replace('.wav', '.json'));
			++jIndex;
		}
	}

	function onError(err) {
		console.log(err);
		process.exit(-1);
	}


	function onClose(code) {
		console.log('Webscoket closed with code ' + code);
		if(code === 1000){
			global.code = 1000;
			console.log('Cleaning Transcriptions');
			cleaner.startCleaningTranscript(options.dirtyDirPath, options.cleanDirPath);
		} else if(code === 1011) {
			clearInterval(tid);
			global.code = 1011;
			console.log('Cleaning Transcriptions');
			cleaner.startCleaningTranscript(options.dirtyDirPath, options.cleanDirPath);
		}
		else{
			console.log('Something went wrong during the process of transcripting...\nTry running again\nIf the problem still persists, Contact @ vthukral1313@gmail.com');
			process.exit(-1);
		}

	}

}



var endMessage = JSON.stringify({'action': 'stop'});

function readAndSendFile(filePath, ws) {
	fs.readFile(filePath, function(err, data) {
		if(!err){
			console.log('Trying to transcribe the audio at path ', filePath);
			ws.send(data, { binary: true, mask: true });
			ws.send(endMessage);
		}
		else{
			console.log('Something went wrong while reading the file ' + filePath + '.\nERR: ' + err);
			process.exit(-1);
		}
	})
}
