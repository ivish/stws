'use strict';

var watson = require('watson-developer-cloud'),
	socket = require('./socket'),
  path = require('path');

var authorization = watson.authorization({
  username: 'b4018fa1-e278-477c-90ef-9535a4454160',
  password: 'r82sUPws43u8',
  version: 'v1'
});

var params = {
  // URL of the resource you wish to access
  url: 'https://stream.watsonplatform.net/speech-to-text/api'
};

authorization.getToken(params, function(err, token) {
	if(!token){
		console.log(err);
	}
	else{
		var options = {};
		options.token = token;
		options.srcDirPath = '/var/lib/freeswitch/recordings/159.203.174.132';
		options.dirtyDirPath = path.join(__dirname, 'dirtyTranscript');
		options.cleanDirPath = path.join(__dirname, 'cleanTranscript');
    options.backUpPath = '/root/recordingsBackUp';
    global.options = options;
		socket.initSocket(options);
	}
});
