'use strict';

var spawn = require('child_process').spawn,
    getListFromDir = require('./dirFilesList').getListFromDir,
    fs = require('fs-extra'),
    findRemoveSync = require('find-remove'),
    callRecordingRegEx = /rec\d\_[\w\d\-]+\_rec\.wav/;



exports.doIt = doIt;


function doIt() {
  console.log('Starting Cleaning of Workspace.');
  var options = global.options;
  var timeStamp = Date();
  getListFromDir(options.dirtyDirPath)
  .then(function(filesList) {
    findRemoveSync(options.dirtyDirPath, {extensions: ['.json']});
    findRemoveSync(options.cleanDirPath, {extensions: ['.json']});
    console.log('Cleaning Done.\n Starting BackUp of recordings');
    fs.mkdirs(options.backUpPath + '/' + timeStamp, function (err) {
      if (err) {
        console.error(err);
        process.exit(-1);
      }
      fs.copy(options.srcDirPath, options.backUpPath+'/'+timeStamp, function(err){
        if(err){
          console.error(err);
          process.exit(-1);
        }
        filesList = filesList.map(function(elem) {
          return elem.replace('.json', '.wav');
        })
        filesList.forEach(function(file) {
          try {
            fs.removeSync(options.srcDirPath+'/'+file);
            console.log(options.srcDirPath+'/'+file)
          } catch(e){
            console.log(e);
            process.exit(-1);
          }
        });

        if(global.code === 1011) {
          process.exit(13);
        } else {
          process.exit(0);
        }
      });
    });
  });
}
