'use strict';

var fs = require('fs'),
	Promise = require('es6-promise').Promise;

exports.getListFromDir = getListFromDir;


/*
 *	The function will compute the list of top level files
 *  at a given path.
 */


function getListFromDir(srcDirPath /* absolute path */) {
	
	return new Promise(function(resolve, reject) {
		fs.access(srcDirPath, fs.F_OK, function(err) {
			if(!err){
				fs.readdir(srcDirPath, function(err, files) {
					if(!err){
						resolve(files);
					}
					else{
						reject('Something went wrong while reading the directory ' + srcDirPath + '.\nERR: ' + err);
					}
				})
			}
			else{
				reject('Something went wrong while accessing the path ' + srcDirPath + '.\nERR: ' + err);
			}
		});
	});
}

