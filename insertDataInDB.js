'use strict';

var pg = require('pg'),
    getListFromDir = require('./dirFilesList').getListFromDir,
    fs = require('fs'),
    path = require('path'),
    cleanWorkspace = require('./cleanWorkspace'),
    transcriptJS = /rec\d\_([\w\d\-]+)\_rec\.json/,
    ut = {};

var cleanDir = '';

function init(cleanD) {
    console.log('Global code: ',global.code);
    console.log('Initialised DB');
    cleanDir = cleanD;
    getListFromDir(cleanD)
        .then(function (filesList) {
            var cleanFilesList = filesList.filter(function (elem, index, arr) {
                if (transcriptJS.test(elem)) {
                    return true;
                }
                else {
                    return false;
                }
            });
            cleanFilesList = cleanFilesList.map(function(elem) {
                elem = elem.split('_');
                elem[2] = elem[0];
                elem[0] = 'rec';
                return elem.join('_');
            });

            cleanFilesList.sort()

            cleanFilesList = cleanFilesList.map(function(elem) {
                elem = elem.split('_');
                elem[0] = elem[2];
                elem[2] = 'rec.json';
                return elem.join('_');
            });
            preProcess(cleanFilesList);

            //insertInDB(cleanFilesList)
        }, function (err) {
            console.log(err);
        });
}

function preProcess(sortedList) {
    var filesCount = sortedList.length;
    var i = 0;
    while (i < filesCount) {
        var rec1 = sortedList[i];

        var resultArr = rec1.match(transcriptJS);

        if (resultArr.length > 1) {
            var uuid = resultArr[1],
                resultTranscript = '',
                upToIndex = i;
            sortedList.every(function (val, index) {
                if (index > i || index == filesCount - 1) {
                    if((index == filesCount - 1) && (val.indexOf(uuid) != -1) ) {
                        upToIndex = index;
                        return false;
                    }
                    if (val.indexOf(uuid) == -1) {
                        upToIndex = index - 1;
                        return false;
                    }
                    else {
                        return true;
                    }
                } else {
                    return true;
                }
            });

            while (i <= upToIndex) {
                var rec = sortedList[i];
                var transcript = readAndReturnTranscript(rec);
                if (transcript && transcript.result) {
                    resultTranscript += transcript.result.join(' ');
                    resultTranscript += '|';
                }
                ++i;
            }
            if (uuid && resultTranscript) {
                ut[uuid] = resultTranscript;
            }
        }
        else {
            ++i;
        }
        uuid = resultTranscript = null;
    }
    insertInDB();
}

function readAndReturnTranscript(fileName) {
    return JSON.parse(fs.readFileSync(path.join(cleanDir, fileName), 'utf-8').toString());
}


function insertInDB() {
  console.log('Starting DB Insertion');
  var config = {
    user: 'fusionpbx', //env var: PGUSER
    database: 'fusionpbx', //env var: PGDATABASE
    password: '/+TxyHvl/l9JirOEKoW2f+HSqow=', //env var: PGPASSWORD
    host: 'localhost', // Server hosting the postgres database
    port: 5432, //env var: PGPORT
    max: 10, // max number of clients in the pool
    idleTimeoutMillis: 30000,
  },
  pool = new pg.Pool(config);

  pool.connect(function (err, client, done) {
      if (err) {
          console.error('error fetching client from pool', err);
          process.exit(-1);
      }
      else {
          console.log('Connected to DB');
          var keysCount = Object.keys(ut).length
          if(keysCount === 0) {
            console.log('Nothing to insert in DB');
            cleanWorkspace.doIt();
          }
          for (var uuid in ut) {
              var transcriptText = ut[uuid];
              console.log('transcriptText *************', transcriptText, 'uuid *************', uuid);
              client.query('UPDATE v_xml_cdr SET recording_file=$1 WHERE uuid=$2 ;', [transcriptText, uuid], function (err, result) {
                  --keysCount;
                  if (err) {
                      console.log('Error occured with ', uuid);
                  }
                  if(keysCount == 0) {
                    cleanWorkspace.doIt();
                  }
              });
          }
      }
  });
}


exports.init = init;
